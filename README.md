# Rock, Paper, Scissors

This coding of the game is based on:
  - DDD: the game is in the core and it defines port to communicate with the rest outside the core
  - User Interface: Cli, Swing, FakeUI (the implementation to test UI - currently only the scafolding)
  - GestureRepository: Based of any kind of game gestures. Now it's implemented RockPaperRespository, but it's very to extend
  - GameModesRespository: repository with the diferents mode games, currently PlayerVsComputer, ComputerVsComputer. But it's prepared to extends very easyly.
  - TDD
  - GameService: it's allow to alocate the game as a service, whereever you like
  - GameRunner: it's a common runner shared for all the differents user interfaces flavors of the games
  - Lauchers: In order to run the game. CliGameLauncher (Commnand Line Interface), SwingGameLauncher (Swing GUI Interface)
  - ...

In order to execute:
  - Cli version: mvn exec:java -Dexec.mainClass="CliGameLauncher"
  - Swing version: mvn exec:java -Dexec.mainClass="SwingGameLauncher"
  - Testing: mvn test
  
It's been a litle game, but it has allowed to me to practice a lot of design and architecture chances. But, it's in progress...
