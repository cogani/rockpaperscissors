package messaging;

public interface Query{
	public Object query(Message message);
}
