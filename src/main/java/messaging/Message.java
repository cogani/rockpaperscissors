package messaging;

public class Message {
	final private Type type;
	final private Object value;

	public Message(Type type) {
		this(type, null);
	}

	public Message(Type type, Object value) {
		this.type = type;
		this.value = value;
	}

	public Type getType() {
		return type;
	}

	public Object getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Message other = (Message) obj;
		if (type != other.type)
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public enum Type {
		GET_GAME_MODE, GET_PLAYER_GESTURE, GAME_MODES_HAS_BEEN_SELECTED, GESTURE_HAS_BEEN_SELECTED, SHOW_RESULT, PLAY_HAND, TESTING,;
	}
}