package messaging;

public class MessagesMediatorException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 112054442886533132L;
	final private Type type;

	public MessagesMediatorException(Message message, Type type) {
		super(message.getType() + ": " + type);
		this.type = type;
	}

	public Type getType() {
		return type;
	}
	
	public enum Type {
		NOT_QUERY_LISTENER_REGISTER("not query listener registered for this message"),
		NOT_COMMAND_LISTENER_REGISTER("not command listener registered for this message"),
		MESSAGE_NOT_HANDLE_BY_COMMAND_LISTENER("the message is not handle by the registered command listener"),
		MESSAGE_NOT_HANDLE_BY_QUERY_LISTENER("the message is not handle by the registered query listener");

		private String string;

		private Type(String name) {
			string = name;
		}

		public String toString() {
			return string;
		}
	}
}