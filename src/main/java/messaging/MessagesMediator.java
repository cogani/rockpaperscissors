package messaging;

import java.util.HashMap;
import java.util.Map;

public class MessagesMediator implements Command, Query {


	private Map<Message.Type, Query> queryRunners;
	private Map<Message.Type, Command> commandRunners;

	public MessagesMediator() {
		queryRunners = new HashMap<Message.Type, Query>();
		commandRunners = new HashMap<Message.Type, Command>();
	}

	public void addQueryListener(Message.Type messageType, Query queryRunner) {
		queryRunners.put(messageType, queryRunner);
	}

	public void addCommandListener(Message.Type messageType,
			Command commandRunner) {
		commandRunners.put(messageType, commandRunner);
	}

	public Object query(Message message) throws MessagesMediatorException{
		Query messagesListener = getQueryListener(message);
		return messagesListener.query(message);
	}

	public void command(Message message) throws MessagesMediatorException{
		Command messagesListener = getCommandListener(message);
		messagesListener.command(message);
	}

	private Query getQueryListener(Message message){
		Query messagesListener = queryRunners.get(message.getType());
		if (messagesListener == null)
			throw new MessagesMediatorException(message,
					MessagesMediatorException.Type.NOT_QUERY_LISTENER_REGISTER);
		return messagesListener;
	}

	private Command getCommandListener(Message message) {
		Command messagesListener = commandRunners.get(message
				.getType());
		if (messagesListener == null)
			throw new MessagesMediatorException(message,
					MessagesMediatorException.Type.NOT_COMMAND_LISTENER_REGISTER);
		return messagesListener;
	}
}