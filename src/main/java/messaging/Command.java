package messaging;

public interface Command {
	public void command(Message message) throws MessagesMediatorException;
}
