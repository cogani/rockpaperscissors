

import javax.swing.SwingUtilities;

import baseui.UserInterface;
import runner.GameRunner;
import swing.SwingGUI;
import core.gamemode.GesturesRepository;
import core.gesturesrsg.RockPaperScissorsGesturesRepository;

public class SwingGameLauncher {
	public static void main(String[] args) {

		GesturesRepository rockPaperScissorsGesturesRepository = new RockPaperScissorsGesturesRepository();
		UserInterface swingGUI = new SwingGUI();
		
		// rpsSwing -> Rock Paper Scissors by Swing interface

		final GameRunner rpsSwing = new GameRunner(rockPaperScissorsGesturesRepository,
				swingGUI);
		
		 SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	            	rpsSwing.show();
	        		rpsSwing.run();
	            }
	        });
	}
}