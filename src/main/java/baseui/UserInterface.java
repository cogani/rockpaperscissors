package baseui;

import runner.HandRunnerCallback;
import core.gamemode.GesturesRepository;
import core.gamemode.ModesRepository;
import messaging.Command;
import messaging.Query;

public abstract class UserInterface {

	protected String nameGame;
	protected ReaderGameModePort readerGameMode;
	protected ReaderPlayerGesturePort readerPlayerGesture;
	final protected DisplayerResultPort displayerResult;

	public UserInterface(DisplayerResultPort displayerResult) {
		this.displayerResult = displayerResult;
	}

	public abstract void prepareSubUI(ModesRepository modesRepository,
			GesturesRepository gesturesRepository);

	public Query getReaderPlayerGesture() {
		return readerPlayerGesture;
	}

	public Query getReaderGameMode() {
		return readerGameMode;
	}

	public Command getDisplayerResult() {
		return displayerResult;
	}

	public abstract void show();

	public abstract void run(HandRunnerCallback runnerHand);

}