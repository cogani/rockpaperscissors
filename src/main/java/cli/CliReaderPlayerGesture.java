package cli;

import baseui.ReaderPlayerGesturePort;
import core.gamemode.GesturesRepository;
import messaging.Message;

public class CliReaderPlayerGesture implements ReaderPlayerGesturePort{

	private GesturesRepository gesturesRepository;
	private CliIO cliIO;

	public CliReaderPlayerGesture(GesturesRepository gesturesRepository, CliIO cliIO) {
		this.gesturesRepository =gesturesRepository;
		this.cliIO = cliIO;
	}

	public Object query(Message message) {
		cliIO.printMenu(gesturesRepository);
		return gesturesRepository.get((Integer) cliIO.readIntOptionFromKeyboard());
	}
}