package cli;

import baseui.DisplayerResultPort;
import messaging.Message;
import messaging.MessagesMediatorException;
import core.gesturesrsg.Result;

public class CliDisplayerResult implements DisplayerResultPort{
	
	public void command(Message message) throws MessagesMediatorException {
		show((Result)message.getValue());
	}
	
	private void show(Result result) {
		System.out.println(result);
	}
}