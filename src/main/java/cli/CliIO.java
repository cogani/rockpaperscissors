package cli;

import java.util.Scanner;

import core.TitledList;

public class CliIO {
	public String readStringFromKeyboard() {
		@SuppressWarnings("resource")
		Scanner keyboard = new Scanner(System.in);
		return keyboard.nextLine();
	}
	
	public int readIntOptionFromKeyboard() {
		@SuppressWarnings("resource")
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Option:");
		return keyboard.nextInt();
	}

	public <E> void printMenu(TitledList<E> items) {
		int i = 0;
		System.out.println("_______________________________");
		System.out.println(items.getTitle());
		System.out.println("-------------------------------");
		for (E item : items.getItems()) {
			System.out.println(i + ". " + item.toString());
			i++;
		}
	}
	
	public void printMessage(String message) {
		System.out.println(message);
	}
}
