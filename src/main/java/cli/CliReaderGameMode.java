package cli;

import baseui.ReaderGameModePort;
import core.gamemode.ModesRepository;
import messaging.Message;

public class CliReaderGameMode implements ReaderGameModePort{
	private ModesRepository modesRepository ;
	private CliIO cliIO;

	public CliReaderGameMode(ModesRepository modesRepository, CliIO cliIO) {
		this.modesRepository =modesRepository;
		this.cliIO = cliIO;
	}

	public Object query(Message message) {
		cliIO.printMenu(modesRepository);
		int option = cliIO.readIntOptionFromKeyboard();
		return modesRepository.get(option);
	}
}
