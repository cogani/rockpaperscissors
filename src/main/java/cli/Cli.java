package cli;

import runner.HandRunnerCallback;
import baseui.UserInterface;
import core.gamemode.GesturesRepository;
import core.gamemode.ModesRepository;

public class Cli extends UserInterface {

	private CliIO cliIO;

	public Cli() {
		super(new CliDisplayerResult());
		cliIO = new CliIO();
	}

	public void prepareSubUI(ModesRepository modesRepository,
			GesturesRepository gesturesRepository) {
		nameGame = gesturesRepository.getTitle();
		readerPlayerGesture = new CliReaderPlayerGesture(gesturesRepository,
				cliIO);
		readerGameMode = new CliReaderGameMode(modesRepository, cliIO);
	}

	public void show() {
		System.out.println(nameGame + " hand game");
		System.out.println("-------------------------------");
	}

	@Override
	public void run(HandRunnerCallback runnerHand) {
		while (true) {
			runnerHand.playHand();
			System.out.println("-------------------------------");
			cliIO.printMessage("Continue playing (S/N)?");
			
			String upperCaseOption = cliIO.readStringFromKeyboard().toUpperCase().trim();
			if(upperCaseOption.equals("S")==false)
				System.exit(0);
		}
	}

}