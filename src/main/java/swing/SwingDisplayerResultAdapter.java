package swing;

import java.awt.Container;

import javax.swing.JOptionPane;

import baseui.DisplayerResultPort;
import messaging.Message;
import messaging.MessagesMediatorException;
import core.gesturesrsg.Result;

public class SwingDisplayerResultAdapter implements DisplayerResultPort{
	
	private Container rootPanel; 

	public SwingDisplayerResultAdapter(Container rootPanel) {
		this.rootPanel = rootPanel;
	}

	public void show(Result result) {
		Scoreboard scoreboard = new Scoreboard("Result", result.toString());
		
		JOptionPane.showMessageDialog(rootPanel,
				scoreboard.getMessage(), scoreboard.getTitle(),
				JOptionPane.INFORMATION_MESSAGE);
	}

	public void command(Message message) throws MessagesMediatorException {
		show((Result) message.getValue());
		
	}
}