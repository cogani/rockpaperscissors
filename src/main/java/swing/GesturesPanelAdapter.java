package swing;

import java.awt.FlowLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;

import baseui.ReaderPlayerGesturePort;
import core.TitledList;
import core.gesturesrsg.BaseGesture;
import messaging.Message;
import messaging.MessagesMediator;
import messaging.MessagesMediatorException;
import messaging.MessagesMediatorException.Type;

public class GesturesPanelAdapter extends JPanel implements ReaderPlayerGesturePort{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3342356348690163896L;
	private ButtonGroup gesturesGroup;

	private TitledList<BaseGesture> gesturesSetGame;
	private MessagesMediator messagesMediator;

	public GesturesPanelAdapter(MessagesMediator messagesRouter,
			TitledList<BaseGesture> gesturesSetGame) {
		super();

		initModel(messagesRouter, gesturesSetGame);
		createGesturesPanel();
	}

	private void initModel(MessagesMediator messagesRouter,
			TitledList<BaseGesture> gesturesSetGame){
		this.messagesMediator = messagesRouter;
		this.gesturesSetGame = gesturesSetGame;
	}

	private void createGesturesPanel() {
		this.setLayout(new FlowLayout());
		setupTitle();
		setupGestureOptions();
	}

	private void setupTitle() {
		JLabel yourGestureTitle = new JLabel("Your gesture:");
		add(yourGestureTitle);
	}

	private void setupGestureOptions() {
		GestureListener gestureListener = new GestureListener();
		List<String> gestureNames = gesturesSetGame.getItemsNamesList();

		gesturesGroup = new ButtonGroup();

		for (int i = 0; i < gestureNames.size(); i++) {

			JRadioButtonWithDataModel<BaseGesture> gesture = new JRadioButtonWithDataModel<BaseGesture>(
					(BaseGesture)gesturesSetGame.get(i), gestureNames.get(i));
			gesture.addItemListener(gestureListener);
			add(gesture);
			gesturesGroup.add(gesture);
		}
	}

	class GestureListener implements ItemListener {

		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == ItemEvent.SELECTED)
				messagesMediator.command(new Message(Message.Type.GESTURE_HAS_BEEN_SELECTED));
		}
	}

	public BaseGesture getActiveGesture() {
		return getSelectedGestureButton().getDataModel();
	}

	void reset() {
		gesturesGroup.clearSelection();
	}
	
	private JRadioButtonWithDataModel<BaseGesture> getSelectedGestureButton() {
		JRadioButtonWithDataModel<BaseGesture> jRadioButtonWithDataModel = (JRadioButtonWithDataModel<BaseGesture>) GroupButtonUtils
				.getFirstSelectedButton(gesturesGroup);
		return jRadioButtonWithDataModel;
	}

	// Query MessagesListener implementations

	public BaseGesture query(Message message) {
		switch (message.getType()) {
		case GET_PLAYER_GESTURE:
			return getSelectedGestureButton().getDataModel();
		default:
			throw new MessagesMediatorException(message,
					Type.MESSAGE_NOT_HANDLE_BY_QUERY_LISTENER);
		}
	}

	public BaseGesture getGesture() {
		return getSelectedGestureButton().getDataModel();
	}
}