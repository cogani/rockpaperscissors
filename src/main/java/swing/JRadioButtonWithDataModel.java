package swing;

import javax.swing.JRadioButton;

public class JRadioButtonWithDataModel<E> extends JRadioButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4609678316503278449L;
	private E dataModel;

	public JRadioButtonWithDataModel(E dataModel, String text) {
		super(text);
		this.setDataModel(dataModel);
	}

	public E getDataModel() {
		return dataModel;
	}

	public void setDataModel(E dataModel) {
		this.dataModel = dataModel;
	}
}
