package swing;

public class Scoreboard {
	final private String title;
	final private String message;
	
	public Scoreboard(String title, String message) {
		this.title = title;
		this.message = message;
	}
	
	public String getTitle() {
		return title;
	}

	public String getMessage() {
		return message;
	}
}