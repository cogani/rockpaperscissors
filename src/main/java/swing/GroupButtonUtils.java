package swing;

import java.util.Enumeration;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;

public class GroupButtonUtils {

    public static String getFirstSelectedButtonText(ButtonGroup buttonGroup) {
    	AbstractButton abstractButton = getFirstSelectedButton(buttonGroup);

    	if(abstractButton!=null)
    		return abstractButton.getText();
    	else return null;
    }
    
    public static AbstractButton getFirstSelectedButton(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button;
            }
        }
        return null;
    }
}