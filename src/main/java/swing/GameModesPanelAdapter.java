package swing;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;

import baseui.ReaderGameModePort;
import core.gamemode.GameMode;
import core.gamemode.ModesRepository;
import messaging.Message;
import messaging.MessagesMediator;
import messaging.MessagesMediatorException;
import messaging.MessagesMediatorException.Type;

public class GameModesPanelAdapter extends JPanel implements  ReaderGameModePort {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4141779757841369337L;
	private ButtonGroup gameModesGroup;
	private List<GameMode> gameModes;
	private ModesRepository modesRepository;
	private MessagesMediator guiMessagesMediator;

	public GameModesPanelAdapter(MessagesMediator guiMessagesMediator, ModesRepository modesRepository) {
		super();
		initModel(guiMessagesMediator, modesRepository);
		createGameModesPanel();
	}
	
	private void initModel(MessagesMediator guiMessagesMediator, ModesRepository modesRepository){
		this.guiMessagesMediator = guiMessagesMediator;
		this.modesRepository = modesRepository;
		gameModes = modesRepository.getItems();
	}

	private void createGameModesPanel() {
		setLayout(new GridLayout(2, 1));
		
		setTitle();
		setGameModesOptions();
	}

	private void setTitle() {
		JLabel titleModesLabel = new JLabel(modesRepository.toString(), JLabel.CENTER);
		add(titleModesLabel);
	}

	private void setGameModesOptions() {
		JPanel optionsGameModePanel = new JPanel(new FlowLayout());
		final GameModesListener gameModesListener = new GameModesListener();

		gameModesGroup = new ButtonGroup();

		for (int i = 0; i < gameModes.size(); i++) {

			JRadioButtonWithDataModel<GameMode> gameMode = new JRadioButtonWithDataModel<GameMode>(
					(GameMode) gameModes.get(i), gameModes.get(i).toString());
			gameMode.addItemListener(gameModesListener);
			optionsGameModePanel.add(gameMode);
			gameModesGroup.add(gameMode);
		}
		add(optionsGameModePanel);

	}

	class GameModesListener implements ItemListener {
		public void itemStateChanged(ItemEvent e) {
			if (e.getStateChange() == ItemEvent.SELECTED)
				guiMessagesMediator.command(new Message(
						Message.Type.GAME_MODES_HAS_BEEN_SELECTED,
						queryGameMode()));
		}
	}

	void reset() {
		gameModesGroup.clearSelection();
	}

	private GameMode queryGameMode() {
		JRadioButtonWithDataModel<GameMode> selectedGameMode = (JRadioButtonWithDataModel<GameMode>) GroupButtonUtils
				.getFirstSelectedButton(gameModesGroup);
		return selectedGameMode.getDataModel();
	}
	
	// Query MessagesListener implementations

	public Object query(Message message) {

		switch (message.getType()) {
		case GET_GAME_MODE:
			return queryGameMode();
		default:
			throw new MessagesMediatorException(message,
					Type.MESSAGE_NOT_HANDLE_BY_QUERY_LISTENER);
		}
	}

	public GameMode getGameMode() {
		return queryGameMode();
	}
}