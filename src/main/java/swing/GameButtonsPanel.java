package swing;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import messaging.Message;
import messaging.MessagesMediator;

public class GameButtonsPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private MessagesMediator messagesMediator;
	private JButton handButton;

	public GameButtonsPanel(MessagesMediator messagesMediator) {
		super();
		init(messagesMediator);
		createWidgets();
	}

	private void init(MessagesMediator messagesMediator) {
		this.messagesMediator = messagesMediator;
	}

	private void createWidgets() {
		setLayout(new FlowLayout());
		handButton = new JButton("Play hand!");
		handButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				messagesMediator.command(new Message(Message.Type.PLAY_HAND));
			}
		});
		add(handButton);
	}
}