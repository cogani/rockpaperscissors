package swing;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

import runner.HandRunnerCallback;
import baseui.ReaderPlayerGesturePort;
import baseui.UserInterface;
import messaging.Command;
import messaging.Message;
import messaging.MessagesMediator;
import messaging.MessagesMediatorException;
import core.gamemode.GameMode;
import core.gamemode.GesturesRepository;
import core.gamemode.ModesRepository;

public class SwingGUI extends UserInterface {

	private JFrame mainFrame;
	private MessagesMediator guiMessagesMediator;
	private GameButtonsPanel gameButtonsPanel;
	private HandRunnerCallback runnerHand;

	public SwingGUI() {
		super(new SwingDisplayerResultAdapter(null));
		createMainFrame();
		guiMessagesMediator = new MessagesMediator();

		// show();
	}

	public void prepareSubUI(ModesRepository modesRepository,
			GesturesRepository gesturesRepository) {
		nameGame = gesturesRepository.getTitle();
		mainFrame.setTitle(nameGame);
		readerGameMode = new GameModesPanelAdapter(guiMessagesMediator,
				modesRepository);
		readerPlayerGesture = (ReaderPlayerGesturePort) new GesturesPanelAdapter(
				guiMessagesMediator, gesturesRepository);
		gameButtonsPanel = new GameButtonsPanel(guiMessagesMediator);

		mainFrame.add((Component) readerGameMode);
		mainFrame.add((Component) readerPlayerGesture);
		mainFrame.add(gameButtonsPanel);
		resetGUI();
		addGUIMessagesListener();
	}

	private void resetGUI() {
		((GameModesPanelAdapter) readerGameMode).reset();
		((Component) readerPlayerGesture).setVisible(false);
		gameButtonsPanel.setVisible(false);
	}

	private void createMainFrame() {
		mainFrame = new JFrame(nameGame);

		mainFrame.setSize(400, 200);
		mainFrame.setLayout(new GridLayout(3, 1));

		mainFrame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent windowEvent) {
				System.exit(0);
			}
		});
	}

	public void show() {
		// resetGUI();
		mainFrame.setVisible(true);
	}

	private void addGUIMessagesListener() {
		guiMessagesMediator.addCommandListener(
				Message.Type.GAME_MODES_HAS_BEEN_SELECTED,
				new GamesModesHasBeenSelected());
		guiMessagesMediator.addCommandListener(
				Message.Type.GESTURE_HAS_BEEN_SELECTED,
				new GesturesHasBeenSelected());
		guiMessagesMediator.addCommandListener(Message.Type.PLAY_HAND,
				new PlayHandListener());
	}

	class GamesModesHasBeenSelected implements Command {
		public void command(Message gameModeSelected) {
			GameMode gameModeLocal = ((GameMode) gameModeSelected
					.getValue());
			boolean isGestureInteractive = gameModeLocal.isGestureInteractive();
			setUpWidgetState(isGestureInteractive);
			
		}
		
		private void setUpWidgetState(boolean isGestureInteractive){
			if (isGestureInteractive == false) {
				((Component) gameButtonsPanel).setVisible(true);
				((Component) readerPlayerGesture).setVisible(false);
			} else {
				((GesturesPanelAdapter)readerPlayerGesture).reset();
				((Component) readerPlayerGesture).setVisible(true);
				((Component) gameButtonsPanel).setVisible(false	);
			}
		}
	}

	class GesturesHasBeenSelected implements Command {
		public void command(Message message) {
			gameButtonsPanel.setVisible(true);
		}
	}

	public class PlayHandListener implements Command {

		public void command(Message message) throws MessagesMediatorException {
			runnerHand.playHand();
			resetGUI();
		}
	}

	@Override
	public void run(HandRunnerCallback runnerHand) {
		this.runnerHand = runnerHand;

	}
}