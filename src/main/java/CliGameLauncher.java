
import runner.GameRunner;
import baseui.UserInterface;
import cli.Cli;
import core.gamemode.GesturesRepository;
import core.gesturesrsg.RockPaperScissorsGesturesRepository;

public class CliGameLauncher {
	public static void main(String[] args) {

		final GesturesRepository rockPaperScissorsGesturesRepository = new RockPaperScissorsGesturesRepository();
		final UserInterface textConsoleUI = new Cli();

		// rpsCli -> Rock Paper Scissors by Cli interface

		GameRunner rpsCli = new GameRunner(
				rockPaperScissorsGesturesRepository, textConsoleUI);

		rpsCli.show();
		rpsCli.run();
	}
}