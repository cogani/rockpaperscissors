package core.gesturesrsg;

import core.Tie;
import core.Victory;


public abstract class BaseGesture{
	private String name;
	
	public BaseGesture(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public Result winAgainst(BaseGesture other){
		return new Victory(this, other);
	}
	
	public Tie tieWith(BaseGesture other){
		return new Tie(other);
	}

	public abstract Result playAgainst(BaseGesture gesture);
	public  abstract Result playAgainstPaper(Paper paper);
	public  abstract Result playAgainstRock(Rock rock);
	public  abstract Result playAgainstScissors(Scissors scissors);

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseGesture other = (BaseGesture) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return name;
	}	
}