package core.gesturesrsg;


public class Scissors extends BaseGesture{

	public Scissors() {
		super("Scissors");
	}

	public Result playAgainst(BaseGesture other) {
		return other.playAgainstScissors(this);
	}

	public Result playAgainstPaper(Paper paper) {
		return winAgainst(paper);
	}

	public Result playAgainstScissors(Scissors scissors) {
		return tieWith(scissors);
	}

	public Result playAgainstRock(Rock rock) {
		return rock.playAgainstScissors(this);
	}
}