package core.gesturesrsg;

import core.gamemode.GesturesRepository;

public class RockPaperScissorsGesturesRepository extends GesturesRepository {

	public RockPaperScissorsGesturesRepository() {
		super("Rock, Paper, Scissors");
		fillUpGestureSetName();
	}

	public BaseGesture getGestureByIndex(int index) {
		return (BaseGesture) get(index);
	}

	private void fillUpGestureSetName() {
		add(new Paper());
		add(new Rock());
		add(new Scissors());
	}
}