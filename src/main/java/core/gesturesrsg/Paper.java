package core.gesturesrsg;

import core.Tie;


public class Paper extends BaseGesture {

	public Paper() {
		super("Paper");
	}

	public Result playAgainst(BaseGesture other) {
		return other.playAgainstPaper(this);
	}

	public Tie playAgainstPaper(Paper paper) {
		return tieWith(paper);
	}

	public Result playAgainstScissors(Scissors scissors) {
		return scissors.playAgainstPaper(this);
	}

	public Result playAgainstRock(Rock rock) {
		return winAgainst(rock);
	}
}