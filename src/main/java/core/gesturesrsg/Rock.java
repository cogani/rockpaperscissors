package core.gesturesrsg;


public class Rock extends BaseGesture {

	public Rock() {
		super("Rock");
	}

	public Result playAgainst(BaseGesture other) {
		return other.playAgainstRock(this);
	}

	public Result playAgainstPaper(Paper paper) {
		return paper.playAgainstRock(this);
	}

	public Result playAgainstScissors(Scissors scissors) {
		return winAgainst(scissors);
	}

	public Result playAgainstRock(Rock rock) {
		return tieWith(rock);
	}
	
	
}