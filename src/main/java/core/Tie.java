package core;

import core.gesturesrsg.BaseGesture;
import core.gesturesrsg.Result;


public class Tie extends Result{
	final private BaseGesture gesture;

	public Tie(BaseGesture gesture) {
		super();
		this.gesture = gesture;
	}

	public BaseGesture getGesture() {
		return gesture;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gesture == null) ? 0 : gesture.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tie other = (Tie) obj;
		if (gesture == null) {
			if (other.gesture != null)
				return false;
		} else if (!gesture.equals(other.gesture))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "2 " + gesture.toString() +"s ties";
	}
	
	
}