package core;

import core.gesturesrsg.BaseGesture;

public class Hand {
	private final BaseGesture gesture1, gesture2;

	public Hand(BaseGesture gesture1, BaseGesture gesture2) {
		this.gesture1 = gesture1;
		this.gesture2 = gesture2;
	}
	
	public BaseGesture getGesture1() {
		return gesture1;
	}

	public BaseGesture getGesture2() {
		return gesture2;
	}
}
