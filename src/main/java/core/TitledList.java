package core;

import java.util.ArrayList;
import java.util.List;

public abstract class TitledList<E> {
	final private String title;
	final private List<E> items;

	protected TitledList(String title) {
		this.title = title;
		items = new ArrayList<E>();
	}

	public int size() {
		return items.size();
	}

	public E get(int itemIndex) {
		return items.get(itemIndex);
	}

	public String getTitle() {
		return title;
	}

	public List<String> getItemsNamesList() {
		List<String> itemsName = new ArrayList<String>();

		for (int i = 0; i < items.size(); i++) {
			String gestureName = items.get(i).toString();
			itemsName.add(gestureName);
		}
		return itemsName;
	}

	public void add(E newItem) {
		items.add(newItem);
	}

	public final List<E> getItems() {
		return items;
	}

	@Override
	public String toString() {
		return title;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TitledList<E> other = (TitledList<E>) obj;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}
}