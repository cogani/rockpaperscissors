package core.gesturesgenerator;

import java.util.Random;

import core.gamemode.GesturesRepository;
import core.gesturesrsg.BaseGesture;

public class RandomGestureGenerator extends GestureGenerator {

	private GesturesRepository gesturesSetGame;
	private Random randomGenerator;

	public RandomGestureGenerator(GesturesRepository gesturesSetGame) {
		super(false);
		this.gesturesSetGame = gesturesSetGame;
		randomGenerator = new Random();
	}

	public BaseGesture gesture() {
		int gestureIndex = randomGenerator.nextInt(gesturesSetGame.size());
		return (BaseGesture) gesturesSetGame.get(gestureIndex);
	}
}