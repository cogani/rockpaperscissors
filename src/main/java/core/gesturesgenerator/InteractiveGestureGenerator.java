package core.gesturesgenerator;

import messaging.Message;
import messaging.Message.Type;
import messaging.MessagesMediator;
import core.gesturesrsg.BaseGesture;

public class InteractiveGestureGenerator extends GestureGenerator {
	private MessagesMediator messagesMediator;

	public InteractiveGestureGenerator(MessagesMediator messagesMediator) {
		super(true);
		this.messagesMediator = messagesMediator;
	}

	public BaseGesture gesture() {
		return (BaseGesture) messagesMediator.query(new Message(
				Type.GET_PLAYER_GESTURE));
	}

}
