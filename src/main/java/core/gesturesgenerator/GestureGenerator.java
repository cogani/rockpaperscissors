package core.gesturesgenerator;

import core.gesturesrsg.BaseGesture;

public abstract class GestureGenerator {
	final private boolean isInteractivo;
	
	public GestureGenerator(boolean isInteractivo) {
		this.isInteractivo = isInteractivo;
	}
	public abstract BaseGesture gesture();
	
	public boolean isInteractive() {
		return isInteractivo;
	}
}
