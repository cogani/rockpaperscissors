package core;

import core.gamemode.GesturesRepository;
import core.gesturesrsg.BaseGesture;
import core.gesturesrsg.Result;

public class GameService {

	private String nameGame;
	private GesturesRepository gesturesRepository;

	public GameService(GesturesRepository gesturesRepository) {
		this.gesturesRepository = gesturesRepository;
		this.nameGame = gesturesRepository.getTitle() + " hand game";
	}
	
	public String getNameGame() {
		return nameGame;
	}

	public TitledList<BaseGesture> getGesturesGame() {
		return gesturesRepository;
	}

	public Result playHand(BaseGesture gesture1, BaseGesture gesture2) {
		Result result = gesture1.playAgainst(gesture2);

		return result;
	}
}