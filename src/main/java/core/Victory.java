package core;

import core.gesturesrsg.BaseGesture;
import core.gesturesrsg.Result;


public class Victory extends Result {
	final private BaseGesture winner;
	final private BaseGesture loser;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((loser == null) ? 0 : loser.hashCode());
		result = prime * result + ((winner == null) ? 0 : winner.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Victory other = (Victory) obj;
		if (loser == null) {
			if (other.loser != null)
				return false;
		} else if (!loser.equals(other.loser))
			return false;
		if (winner == null) {
			if (other.winner != null)
				return false;
		} else if (!winner.equals(other.winner))
			return false;
		return true;
	}

	public Victory(BaseGesture winner, BaseGesture loser) {
		this.winner = winner;
		this.loser = loser;
	}

	public BaseGesture getWinner() {
		return winner;
	}

	public BaseGesture getLoser() {
		return loser;
	}

	@Override
	public String toString() {
		return winner.toString() + " wins to " + loser.toString();
	}

}