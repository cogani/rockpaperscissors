package core.gamemode;

import core.gesturesgenerator.GestureGenerator;
import core.gesturesrsg.BaseGesture;

public class Player {
	final public String type;
	final private GestureGenerator gestureGenerator;
	final private boolean isGestureInteractive;
	
	public Player(String nameType, GestureGenerator gestureGenerator) {
		this.type = nameType;
		this.gestureGenerator = gestureGenerator;
		isGestureInteractive = gestureGenerator.isInteractive();
	}


	public BaseGesture generate(){
		return gestureGenerator.gesture();
	}


	@Override
	public String toString() {
		return type;
	}


	public boolean isGestureInteractive() {
		return isGestureInteractive;
	}
}
