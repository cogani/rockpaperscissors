package core.gamemode;

import core.TitledList;
import core.gesturesrsg.BaseGesture;

public class GesturesRepository extends TitledList<BaseGesture>{

	protected GesturesRepository(String title) {
		super(title);
	}
}