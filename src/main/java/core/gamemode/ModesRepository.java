package core.gamemode;

import core.TitledList;

public class ModesRepository extends TitledList<GameMode> {

	public ModesRepository(String title) {
		super(title);
	}

	public void addMode(Player player1, Player player2) {
		add(new GameMode(player1, player2));
	}
}