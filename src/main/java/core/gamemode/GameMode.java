package core.gamemode;

public class GameMode {
	final private Player player1, player2;
	final private boolean isGestureInteractive;

	public GameMode(Player player1, Player player2) {
		super();
		this.player1 = player1;
		this.player2 = player2;

		isGestureInteractive = player1.isGestureInteractive()
				|| player2.isGestureInteractive() ? true : false;
	}

	public Player getPlayer1() {
		return player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	@Override
	public String toString() {
		return player1.toString() + " vs " + player2.toString();
	}

	public boolean isGestureInteractive() {
		return isGestureInteractive;
	}

}
