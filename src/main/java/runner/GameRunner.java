package runner;



import baseui.UserInterface;
import core.GameService;
import core.gamemode.GesturesRepository;
import core.gamemode.ModesRepository;
import core.gamemode.Player;
import core.gesturesgenerator.GestureGenerator;
import core.gesturesgenerator.InteractiveGestureGenerator;
import core.gesturesgenerator.RandomGestureGenerator;
import messaging.Message.Type;
import messaging.MessagesMediator;

public class GameRunner {
	private MessagesMediator messagesMediator;
	private ModesRepository modesRepository;
	private GesturesRepository gesturesRepository;
	private GestureGenerator interactiveGestureGenerator, randomGestureGenerator;
	private GameService gameService;
	private UserInterface userInterface;
	private HandRunnerCallback handRunnerCallback;

	public GameRunner(GesturesRepository gesturesRepository, UserInterface userInterface) {
		initGesturesRepository(gesturesRepository);
		initCore();
		initUserInterface(userInterface);
		addUserInterfaceListeners();
	}
	
	private void addUserInterfaceListeners() {
		messagesMediator.addQueryListener(Type.GET_GAME_MODE, userInterface.getReaderGameMode());
		messagesMediator.addQueryListener(Type.GET_PLAYER_GESTURE, userInterface.getReaderPlayerGesture());
		messagesMediator.addCommandListener(Type.SHOW_RESULT, userInterface.getDisplayerResult());
	}

	public void initCore() {
		initMessageMediator();
		initGestureGenerators();
		initGameModesRepository();
		initGame(gesturesRepository);
	}

	private void initUserInterface(UserInterface userInterface) {
		this.userInterface = userInterface;
		userInterface.prepareSubUI(modesRepository, gesturesRepository);
	}

	public void run() {
		userInterface.run(handRunnerCallback);
	}
	
	private void initGameModesRepository(){
		Player human, computer;
		human = new Player("Human", interactiveGestureGenerator);
		computer = new Player("Computer", randomGestureGenerator);

		modesRepository = new ModesRepository("Game Modes");
		modesRepository.addMode(human, computer);
		modesRepository.addMode(computer, computer);
	}
	
	private void initGesturesRepository(GesturesRepository gesturesRepository){
		this.gesturesRepository = gesturesRepository;
	}
	
	private void initMessageMediator(){
		messagesMediator = new MessagesMediator();
		
	}
	
	private void initGestureGenerators(){
		interactiveGestureGenerator = new InteractiveGestureGenerator(messagesMediator);
		randomGestureGenerator = new RandomGestureGenerator(gesturesRepository);
	}
	
	private void initGame(GesturesRepository gesturesRepository) {
		gameService = new GameService(gesturesRepository);
		handRunnerCallback = new HandRunnerCallback(messagesMediator, gameService);
	}

	public void show() {
		userInterface.show();
	}
}