package runner;

import messaging.Message;
import messaging.Message.Type;
import messaging.MessagesMediator;
import core.GameService;
import core.gamemode.GameMode;
import core.gesturesrsg.BaseGesture;
import core.gesturesrsg.Result;

public class HandRunnerCallback {
	private final MessagesMediator messagesMediator;
	private final GameService game;
	
	public HandRunnerCallback(MessagesMediator messagesMediator, GameService game) {
		super();
		this.messagesMediator = messagesMediator;
		this.game = game;
	}

	public void playHand(){
		GameMode gameModeLocal = (GameMode) messagesMediator.query(new Message(Type.GET_GAME_MODE));
		BaseGesture gesture1 = gameModeLocal.getPlayer1().generate();
		BaseGesture gesture2 = gameModeLocal.getPlayer2().generate();

		Result result = game.playHand(gesture1, gesture2);
		
		messagesMediator.command(new Message(Type.SHOW_RESULT, result));
	}
}