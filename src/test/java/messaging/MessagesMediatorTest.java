package messaging;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import messaging.Command;
import messaging.Message;
import messaging.Message.Type;
import messaging.MessagesMediator;
import messaging.MessagesMediatorException;
import messaging.Query;

public class MessagesMediatorTest {
	

	@Test
	public void querying_preregister_testing_message_will_be_convenient_responsed() {
		MessagesMediator messagesMediator= new MessagesMediator();
		String expectedResponse = "Test Ok";
		Message sendingMessage = new Message(Type.TESTING);
		
		Query messagesListenerMock=mock(Query.class);
		when(messagesListenerMock.query(sendingMessage)).thenReturn(expectedResponse);
		
		messagesMediator.addQueryListener(Type.TESTING, messagesListenerMock);
		String result = (String) messagesMediator.query(sendingMessage);
		
		assertEquals(expectedResponse, result);
	}
	
	@Test
	public void message_to_not_preregister_query_will_throw_MessageMediatorException_NOT_QUERY_LISTENER_REGISTER() {
		MessagesMediator messagesMediator= new MessagesMediator();
		Type typeWithListener = Type.TESTING;
		Type typeWithNoListener = Type.PLAY_HAND;
		Message messageWithNoQueryListener = new Message(typeWithNoListener);
		
		String expectedResponse = "Not care";
		Query queryListenerMock=mock(Query.class);
		when(queryListenerMock.query(messageWithNoQueryListener)).thenReturn(expectedResponse);
		
		messagesMediator.addQueryListener(typeWithListener, queryListenerMock);
		try {
			messagesMediator.query(messageWithNoQueryListener);
		} catch (MessagesMediatorException e) {
			assertEquals(MessagesMediatorException.Type.NOT_QUERY_LISTENER_REGISTER.toString(), e.getType().toString());
		}
	}
	
	@Test
	public void message_to_not_preregister_command_will_throw_MessageMediatorException_NOT_COMMAND_LISTENER_REGISTER() {
		MessagesMediator messagesMediator= new MessagesMediator();
		Type typeWithCommandListener= Type.TESTING;
		Type typeWithNoCommandListener  = Type.PLAY_HAND;
		Message messageWithNoCommandListener = new Message(typeWithNoCommandListener);
		
		Command commandsListenerMock=mock(Command.class);
		
		messagesMediator.addCommandListener(typeWithCommandListener, commandsListenerMock);
		
		try {
			messagesMediator.command(messageWithNoCommandListener);
		} catch (MessagesMediatorException e) {
			assertEquals(MessagesMediatorException.Type.NOT_COMMAND_LISTENER_REGISTER.toString(), e.getType().toString());
		}
	}
	
	@Test
	public void message_to_preregister_command_without_this_query_will_throw_MessageMediatorException_MESSAGE_NOT_HANDLE_BY_COMMAND_LISTENER() {
		MessagesMediator messagesMediator= new MessagesMediator();
		Type typeWithListener = Type.TESTING;
		Type typeWithNoListener = Type.PLAY_HAND;
		Message messageWithNoCommandListener = new Message(typeWithListener);
		
		String expectedResponse = "Not care";
		Command commandListenerRegisteredWithNoMessageHandleMock=mock(Command.class);
		doThrow(new MessagesMediatorException(messageWithNoCommandListener,MessagesMediatorException.Type.MESSAGE_NOT_HANDLE_BY_COMMAND_LISTENER)).when(commandListenerRegisteredWithNoMessageHandleMock).command(messageWithNoCommandListener);
		
		messagesMediator.addCommandListener(typeWithListener, commandListenerRegisteredWithNoMessageHandleMock);
		try {
			messagesMediator.command(messageWithNoCommandListener);
		} catch (MessagesMediatorException e) {
			assertEquals(MessagesMediatorException.Type.MESSAGE_NOT_HANDLE_BY_COMMAND_LISTENER.toString(), e.getType().toString());
		}
	}
	
	@Test
	public void message_to_preregister_query_without_this_query_will_throw_MessageMediatorException_MESSAGE_NOT_HANDLE_BY_QUERY_LISTENER() {
		MessagesMediator messagesMediator= new MessagesMediator();
		Type typeWithListener = Type.TESTING;
		Type typeWithNoListener = Type.PLAY_HAND;
		Message messageWithNoCommandListener = new Message(typeWithListener);
		
		String expectedResponse = "Not care";
		Query queryListenerRegisteredWithNoMessageHandleMock=mock(Query.class);
		doThrow(new MessagesMediatorException(messageWithNoCommandListener,MessagesMediatorException.Type.MESSAGE_NOT_HANDLE_BY_QUERY_LISTENER)).when(queryListenerRegisteredWithNoMessageHandleMock).query(messageWithNoCommandListener);
		
		messagesMediator.addQueryListener(typeWithListener, queryListenerRegisteredWithNoMessageHandleMock);
		try {
			messagesMediator.query(messageWithNoCommandListener);
		} catch (MessagesMediatorException e) {
			assertEquals(MessagesMediatorException.Type.MESSAGE_NOT_HANDLE_BY_QUERY_LISTENER.toString(), e.getType().toString());
		}
		
	}
}