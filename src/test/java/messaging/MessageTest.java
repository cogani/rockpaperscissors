package messaging;

import static org.junit.Assert.*;
import messaging.Message.Type;

import org.junit.Before;
import org.junit.Test;

public class MessageTest {

	private Message.Type testType;

	@Before
	public void setUp() throws Exception {
		testType = Type.TESTING;
	}

	@Test
	public void testing_message_with_no_value() {
		Message helloMessage = new Message(testType);

		assertEquals(testType, helloMessage.getType());
	}

	@Test
	public void testing_message_with_value() {
		String testValue = "World";
		Message helloMessage = new Message(testType, testValue);

		assertEquals(testType, helloMessage.getType());
		assertEquals(testValue, (String) helloMessage.getValue());
	}
}