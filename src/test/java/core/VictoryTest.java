package core;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import core.gesturesrsg.BaseGesture;

public class VictoryTest {

	@Test
	public void init_test() {
		String winnerName = "winner", loserName = "loser";
		BaseGesture winnerMock = mock(BaseGesture.class);
		BaseGesture loserMock = mock(BaseGesture.class);
		when(winnerMock.toString()).thenReturn(winnerName);
		when(loserMock.toString()).thenReturn(loserName);
		
		Victory victory = new Victory(winnerMock, loserMock);
		
		String expected = winnerMock.toString() + " wins to " + loserMock.toString();
		
		assertEquals(expected, victory.toString());
	}
}