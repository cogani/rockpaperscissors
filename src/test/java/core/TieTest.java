package core;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import core.gesturesrsg.BaseGesture;

public class TieTest {


	@Test
	public void init_test() {
		String tiedName = "GestureName";
		BaseGesture gestureMock = mock(BaseGesture.class);
		when(gestureMock.toString()).thenReturn(tiedName);
		
		Tie tie = new Tie(gestureMock);
		
		String expected = "2 " + tiedName +"s ties";
		
		assertEquals(expected, tie.toString());
	}

}
