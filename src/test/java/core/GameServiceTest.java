package core;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import core.GameService;
import core.gamemode.GesturesRepository;
import core.gesturesrsg.BaseGesture;

public class GameServiceTest {

	 private GesturesRepository gesturesRepositoryMock;
	
	 @Before
	 public void setUp() throws Exception {
		 gesturesRepositoryMock = mock(GesturesRepository.class);
	 }

	@Test
	public void initGame_check() {
		String gameName = "Game name";
		when(gesturesRepositoryMock.getTitle()).thenReturn(gameName);

		GameService gameService = new GameService(gesturesRepositoryMock);

		assertEquals(gameName + " hand game", gameService.getNameGame());
		assertEquals(gesturesRepositoryMock, gameService.getGesturesGame());
	}

	@Test
	public void playHand_call_to_BaseGesture_playAgainst() {
		BaseGesture baseGesture1Mock = mock(BaseGesture.class);
		BaseGesture dumpBaseGesture2 = null;
		

		GameService gameService = new GameService(gesturesRepositoryMock);
		gameService.playHand(baseGesture1Mock, dumpBaseGesture2);

		Mockito.verify(baseGesture1Mock, Mockito.times(1)).playAgainst(
				dumpBaseGesture2);
	}
}
