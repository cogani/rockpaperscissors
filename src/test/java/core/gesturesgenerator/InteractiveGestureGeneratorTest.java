package core.gesturesgenerator;

import static org.mockito.Mockito.mock;
import messaging.Message;
import messaging.MessagesMediator;
import messaging.Message.Type;

import org.junit.Test;
import org.mockito.Mockito;

public class InteractiveGestureGeneratorTest {

	@Test
	public void gesture_send_query_message_GET_PLAYER_GESTURE() {
		MessagesMediator messagesMediatorMock = mock(MessagesMediator.class);
		InteractiveGestureGenerator interactiveGestureGenerator = new InteractiveGestureGenerator(messagesMediatorMock);
		interactiveGestureGenerator.gesture();
		
		Mockito.verify(messagesMediatorMock, Mockito.times(1)).query(
				new Message(
						Type.GET_PLAYER_GESTURE));
	}

}