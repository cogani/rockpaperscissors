package core.gesturesgenerator;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;

import core.gamemode.GesturesRepository;

public class RandomGestureGeneratorTest {
	private int maxSizeGesturesRepositoryMock = 2;

	@Test
	public void gesture_send_query_message_GET_PLAYER_GESTURE() {
		GesturesRepository gesturesRepositoryMock = mock(GesturesRepository.class);
		when(gesturesRepositoryMock.size()).thenReturn(maxSizeGesturesRepositoryMock);

		RandomGestureGenerator randomGestureGenerator = new RandomGestureGenerator(
				gesturesRepositoryMock);
		randomGestureGenerator.gesture();

		Mockito.verify(gesturesRepositoryMock, Mockito.times(1)).get(
				Mockito.intThat(new ZeroToTwoRange()));
	}

	class ZeroToTwoRange extends ArgumentMatcher<Integer> {

		@Override
		public boolean matches(Object generated) {
			int intGenerated = ((Integer) generated).intValue();
			return (intGenerated >= 0 && intGenerated < maxSizeGesturesRepositoryMock) ? true : false;
		}
	}
}