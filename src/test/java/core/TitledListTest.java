package core;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TitledListTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void init_test() {
		String title = "Title";
		TitledList<Object> titledList = new TitledList<Object>(title) {
		};

		assertEquals(title, titledList.getTitle());
		assertEquals(0, titledList.getItems().size());
	}

	@Test
	public void adding_one_element_to_list_test() {
		String title = "Title";
		TitledList<Object> titledList = new TitledList<Object>(title) {
		};

		int sizeListPreAdding = titledList.getItems().size();

		Object newItem = new Object();
		titledList.add(newItem);
		int sizeListPostAdding = titledList.getItems().size();

		assertEquals(sizeListPostAdding, sizeListPreAdding + 1);
		assertEquals(newItem, titledList.get(sizeListPostAdding-1));
	}

}