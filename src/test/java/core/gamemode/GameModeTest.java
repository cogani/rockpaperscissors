package core.gamemode;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

public class GameModeTest {

	@Test
	public void to_toString_test() {
		String playerType1 = "Player1", playerType2 = "Player2";
		String expected = playerType1 + " vs " + playerType2;
		Player player1 = mock(Player.class);
		Player player2 = mock(Player.class);
		when(player1.toString()).thenReturn(playerType1);
		when(player2.toString()).thenReturn(playerType2);

		GameMode gameMode = new GameMode(player1, player2);

		assertEquals(expected, gameMode.toString());

	}

	@Test
	public void isGestureInteractive_when_at_leat_one_player_is_gesture_interactive() {
		Player playerGestureInteractiveMock = mock(Player.class);
		Player playerNotGestureInteractiveMock = mock(Player.class);
		when(playerGestureInteractiveMock.isGestureInteractive()).thenReturn(
				true);
		when(playerNotGestureInteractiveMock.isGestureInteractive())
				.thenReturn(false);

		GameMode gameModeWithBothPlayersGestureInteractive = new GameMode(
				playerGestureInteractiveMock, playerGestureInteractiveMock);
		GameMode gameModeWithOnePlayerGestureInteractive = new GameMode(
				playerNotGestureInteractiveMock, playerGestureInteractiveMock);
		GameMode gameModeWithNoPlayersGestureInteractive = new GameMode(
				playerNotGestureInteractiveMock,
				playerNotGestureInteractiveMock);

		assertEquals(true,
				gameModeWithBothPlayersGestureInteractive
						.isGestureInteractive());
		assertEquals(true,
				gameModeWithOnePlayerGestureInteractive.isGestureInteractive());
		assertEquals(false,
				gameModeWithNoPlayersGestureInteractive.isGestureInteractive());

	}

}
