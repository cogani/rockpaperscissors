package core.gamemode;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import core.gesturesgenerator.GestureGenerator;

public class PlayerTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void player_init_test() {
		GestureGenerator gestureGeneratorMock = mock(GestureGenerator.class);
		String playerNameType = "name type";
		Player player = new Player(playerNameType, gestureGeneratorMock);

		assertEquals(playerNameType, player.toString());
		Mockito.verify(gestureGeneratorMock, Mockito.times(1)).isInteractive();

	}

	@Test
	public void player_isGestureInteractive_depends_on_GestureGenerator() {
		GestureGenerator gestureInteractiveGenerator = mock(GestureGenerator.class);
		when(gestureInteractiveGenerator.isInteractive()).thenReturn(true);

		GestureGenerator gestureNotInteractiveGenerator = mock(GestureGenerator.class);
		when(gestureNotInteractiveGenerator.isInteractive()).thenReturn(false);

		Player playerGestureInteractive = new Player(
				"playerGestureInteractive", gestureInteractiveGenerator);
		Player playerNotGestureInteractive = new Player(
				"playerNotGestureInteractive", gestureNotInteractiveGenerator);

		assertEquals(true, playerGestureInteractive.isGestureInteractive());
		assertEquals(false, playerNotGestureInteractive.isGestureInteractive());
	}
	
	@Test
	public void generate_invokes_gesture_generator() {
		GestureGenerator gestureGeneratorMock = mock(GestureGenerator.class);
		String playerNameType = "name type";
		Player player = new Player(playerNameType, gestureGeneratorMock);
		player.generate();

		assertEquals(playerNameType, player.toString());
		Mockito.verify(gestureGeneratorMock, Mockito.times(1)).gesture();
	}

}
