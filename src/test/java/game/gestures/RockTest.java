package game.gestures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import core.Tie;
import core.Victory;
import core.gesturesrsg.Paper;
import core.gesturesrsg.Rock;
import core.gesturesrsg.Scissors;

public class RockTest {
	
	private Paper paper;
	private Rock rock;
	private Scissors scissors;

	@Before
	public void setUp() throws Exception {
		paper = new Paper();
		rock = new Rock();
		scissors = new Scissors();
	}

	@Test
	public void test_scissors_against_paper() {
		Victory expected = new Victory(scissors, paper);
		assertEquals(expected, scissors.playAgainst(paper));
	}
	
	@Test
	public void test_scissors_against_rock() {
		Victory expected = new Victory(rock, scissors);
		assertEquals(expected, scissors.playAgainst(rock));
	}

	@Test
	public void test_scissors_against_scissors() {
		Tie expected = new Tie(scissors);
		assertEquals(expected, scissors.playAgainst(scissors));
	}
}
