package game.gestures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import core.Tie;
import core.Victory;
import core.gesturesrsg.Paper;
import core.gesturesrsg.Rock;
import core.gesturesrsg.Scissors;

public class PaperTest {
	
	private Paper paper;
	private Rock rock;
	private Scissors scissors;

	@Before
	public void setUp() throws Exception {
		paper = new Paper();
		rock = new Rock();
		scissors = new Scissors();
	}

	@Test
	public void test_paper_against_paper() {
		Tie expected = new Tie(paper);
		assertEquals(expected, paper.playAgainst(paper));
	}
	
	@Test
	public void test_paper_against_rock() {
		Victory expected = new Victory(paper, rock);
		assertEquals(expected, paper.playAgainst(rock));
	}

	@Test
	public void test_paper_against_scissors() {
		Victory expected = new Victory(scissors, paper);
		assertEquals(expected, paper.playAgainst(scissors));
	}
}
