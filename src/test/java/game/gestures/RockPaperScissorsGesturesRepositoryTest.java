package game.gestures;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import core.gamemode.GesturesRepository;
import core.gesturesrsg.RockPaperScissorsGesturesRepository;

public class RockPaperScissorsGesturesRepositoryTest {
	
	@Test
	public void initTest() {
		GesturesRepository rockPaperScissorsGesturesSetGame = new RockPaperScissorsGesturesRepository();
		List<String> namesGesture = rockPaperScissorsGesturesSetGame.getItemsNamesList();
		
		assertEquals(3, namesGesture.size());
		assertTrue(namesGesture.contains("Paper"));
		assertTrue(namesGesture.contains("Rock"));
		assertTrue(namesGesture.contains("Scissors"));
	}
}