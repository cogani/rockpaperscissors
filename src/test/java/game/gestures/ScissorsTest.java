package game.gestures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import core.Tie;
import core.Victory;
import core.gesturesrsg.Paper;
import core.gesturesrsg.Rock;
import core.gesturesrsg.Scissors;

public class ScissorsTest {
	
	private Paper paper;
	private Rock rock;
	private Scissors scissors;

	@Before
	public void setUp() throws Exception {
		paper = new Paper();
		rock = new Rock();
		scissors = new Scissors();
	}

	@Test
	public void test_rock_against_paper() {
		Victory expected = new Victory(paper, rock);
		assertEquals(expected, rock.playAgainst(paper));
	}
	
	@Test
	public void test_rock_against_rock() {
		Tie expected = new Tie(rock);
		assertEquals(expected, rock.playAgainst(rock));
	}

	@Test
	public void test_rock_against_scissors() {
		Victory expected = new Victory(rock, scissors);
		assertEquals(expected, rock.playAgainst(scissors));
	}
}